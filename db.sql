create TABLE film(
    id SERIAL PRIMARY KEY,
    genre VARCHAR(255),
    name VARCHAR(255),
    description VARCHAR(255)
);

