const express = require('express');
const filmRouter = require('./routes/film.routes');
const PORT = process.env.PORT || 3000;

const app = express();
app.use(express.json());
app.use('/', filmRouter);

app.listen(PORT, () => console.log(`server started on port ${PORT}...`));
