const db = require('../db');
class FilmController {
    async createFilm(req, res) {
        const { name, genre, description } = req.body;
        const newFilm = await db.query(
            `INSERT INTO film (name, genre, description) values ($1, $2, $3) RETURNING *`,
            [name, genre, description]
        );
        res.json(newFilm.rows[0]);
    }
    async getFilms(req, res) {
        const films = await db.query('SELECT * FROM film');
        res.json(films.rows);
    }
    async getOneFilm(req, res) {
        const id = req.params.id;
        const film = await db.query(`SELECT * FROM film where id = $1`, [id]);
        res.json(film.rows[0]);
    }
    async updateFilm(req, res) {
        const { name, genre, description, id } = req.body;
        const film = await db.query(
            `UPDATE film set name = $1, description = $2, genre = $3 where id = $4 RETURNING *`,
            [name, description, genre, id]
        );
        res.json(film.rows[0]);
    }
    async deleteFilm(req, res) {
        const id = req.params.id;
        const film = await db.query(`DELETE FROM film where id = $1`, [id]);
        res.send(`Item ${id} removed `);
    }
}
module.exports = new FilmController();
