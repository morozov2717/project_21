const Pool = require('pg').Pool;
const pool = new Pool({
    user: 'postgres',
    password: '1234',
    host: 'localhost',
    port: '5432',
    database: 'films_db_nodejs',
});

module.exports = pool;
